package br.senai.sp.info.jucacontrol.config;
/**
 * 
 * Classe de configuração do Spring.
 * 
 * Criado por Murilo Afonso Condutta em 26/03/2018.
 * 
 */

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
@Import({PersistenceConfig.class})
@ComponentScan("br.senai.sp.info.jucacontrol")
public class WebConfig {
	
}

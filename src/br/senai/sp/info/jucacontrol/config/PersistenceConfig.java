package br.senai.sp.info.jucacontrol.config;
/**
 * 
 * Classe de configura��o do Hibernate.
 * 
 * Criado por Murilo Afonso Condutta em 26/03/2018.
 * 
 */

import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;

@Configuration
public class PersistenceConfig {

	/**
	 * 
	 * Cria um dataSource contnedo as informa��es de conex�o com o banco
	 * de dados que ser� utilizado no Hibernate.
	 * 
	 * @return - dataSource.
	 */
	@Bean
	public DataSource getDataSource() {
		
		// Instancia o dataSource
		BasicDataSource dataSource = new BasicDataSource();
		
		// Indica o driver de conex�o do banco, para o framework "traduzir" a linguagem
		dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
		
		// Indica a URL do banco
		dataSource.setUrl("jdbc:mysql://localhost:3306/jucacontrol_mt3?serverTimezone=UTC");
		
		// Indica o usu�rio e senha
		dataSource.setUsername("root");
		dataSource.setPassword("root132");
		
		// Retorna o dataSource para fazer a conex�o
		return dataSource;
		
	}
	
	/**
	 * 
	 * Configura as propriedades do Hibernate.
	 * 
	 * @return - propriedades do Hibernate.
	 */
	public Properties getHibernateProperties() {
		
		// Instancia as propriedades
		Properties props = new Properties();
		
		// Habilita a apresentacao de quais comandos sql o hibernate est� fazendo - esp�cie de debug.
		// Por padr�o � falso, ou seja, apenas mostrar� no console os comandos se estiver true e aparecer aqui.
		props.setProperty("hibernate.show_sql", "true");
		
		// Segredo (Uhuuuuu);
		/*
		 * Como o hibernate deve criar o banco de dados
		 * 
		 * COMANDO : hibernate mappin para (2 = to) data definition language (script sql para definir os dados Ex.: CREATE TABLE, CREATE DATABASE )
		 * 
		 *  - podemos usar:
		 * 
		 * validate :  n�o altera o banco;
		 * update : altera as classes alteradas;
		 * create : cria o banco de dados(dropando dados anteriores);
		 * create-drop : sempre derruba ao terminar a execu��o d aplica��o (session).
		 * 
		 */
		props.setProperty("hibernate.hbm2ddl.auto", "create");
		
		// Dialect - Define qual linguagem do banco o Hibernate usar�
		props.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
		
		return props;
		
	}
	
	// Sess�o no hibernate = conex�o com banco de dados
	
	/**
	 * 
	 * Cria um obejto que abrir� as conex�es com o banco de dados atrav�s do dataSource
	 * e do HibernateProperties que criamos nesta classe.
	 * 
	 * @return - objeto para conex�o.
	 */
	@Bean
	public LocalSessionFactoryBean getSessionFactory() {
		
		// Instancia a fabrica de conex�es
		LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
		
		// D� um set no dataSource
		sessionFactory.setDataSource(getDataSource());
		
		// D� um set nas propriedades do Hibernate
		sessionFactory.setHibernateProperties(getHibernateProperties());
		
		//Onde se encontra os models do sistema
		sessionFactory.setPackagesToScan("br.senai.sp.info.jucacontrol.models");
		
		return sessionFactory;
		
	}
	
}

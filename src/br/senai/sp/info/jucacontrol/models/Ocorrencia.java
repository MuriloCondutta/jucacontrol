package br.senai.sp.info.jucacontrol.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

/**
* 
* Modelo ocorr�ncia.
* 
* @author Murilo Afonso Condutta
* Criada em 26/03/2018.
*/

@Entity
public class Ocorrencia {

	// Atributos
	
	@Id
	private Long id;
	
	@Column(length = 30, nullable = false, unique = false)
	private String titulo;
	
	// Tira-se o length e colocamos @Lob, para deixar infinito o limite do texto
	@Lob // Long object = objeto sem fim
	@Column(nullable = true, unique = false)
	private String descricao;
	
	@Column(nullable = false, unique = false)
	private Date dataAbertura;
	
	@Column(nullable = false, unique = false)
	private Date dataModificacao;
	
	@Column(nullable = true, unique = false)
	private Date dataConclusao;
	
	// -------- Relacionamentos
	
	@ManyToOne // cardinalidade (n, 1) -> muitas ocorrencias para um usu�rio // // [n] ocorrencia <-> [1] tecnico
	@JoinColumn(nullable = true)
	private Usuario tecnico; // quem atendeu
	
	@ManyToOne // [n] ocorrencia <-> [1] emissor
	@JoinColumn(nullable = false)
	private Usuario emissor; // quem abriu
	
	@ManyToOne // [n] ocorrencia <-> [1] categoria
	@JoinColumn(nullable = false)
	private CategoriaOcorrencia categoria; // categoria dela

	// Getters and Setters
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Date getDataAbertura() {
		return dataAbertura;
	}

	public void setDataAbertura(Date dataAbertura) {
		this.dataAbertura = dataAbertura;
	}

	public Date getDataModificacao() {
		return dataModificacao;
	}

	public void setDataModificacao(Date dataModificacao) {
		this.dataModificacao = dataModificacao;
	}

	public Date getDataConclusao() {
		return dataConclusao;
	}

	public void setDataConclusao(Date dataConclusao) {
		this.dataConclusao = dataConclusao;
	}

	public Usuario getTecnico() {
		return tecnico;
	}

	public void setTecnico(Usuario tecnico) {
		this.tecnico = tecnico;
	}

	public Usuario getEmissor() {
		return emissor;
	}

	public void setEmissor(Usuario emissor) {
		this.emissor = emissor;
	}

	public CategoriaOcorrencia getCategoria() {
		return categoria;
	}

	public void setCategoria(CategoriaOcorrencia categoria) {
		this.categoria = categoria;
	}
	
	
	
}

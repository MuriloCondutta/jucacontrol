package br.senai.sp.info.jucacontrol.models;
/**
* 
* Modelo usu�rio.
* 
* @author Murilo Afonso Condutta
* Criada em 26/03/2018.
*/

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * 
 * Entity - marca que esta � um entidade mapeada pelo Hibernate.
 *
 */
@Entity
public class Usuario {

	// Atributos
	
	@Id // Marca o campo como coluna de identifica��o
	private Long id;
	
	@Column(length = 30, nullable = false, unique = false)
	private String nome;
	
	@Column(length = 50, nullable = false, unique = false)
	private String sobrenome;
	
	@Column(length = 120, nullable = false, unique = false)
	private String email;
	
	// 32 por conta da criptografia de 32 caract�res
	@Column(length = 32, nullable = false, unique = true)
	private String senha;
	
	@Column(nullable = false, unique = false)
	private TiposUsuario tipo;
		
	//Getters and Setters
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSobrenome() {
		return sobrenome;
	}
	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public TiposUsuario getTipo() {
		return tipo;
	}
	public void setTipo(TiposUsuario tipo) {
		this.tipo = tipo;
	}
	
	
}

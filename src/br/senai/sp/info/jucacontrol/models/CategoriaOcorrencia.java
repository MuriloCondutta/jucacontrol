package br.senai.sp.info.jucacontrol.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
* 
* Modelo categoriaOcorrencia.
* 
* @author Murilo Afonso Condutta
* Criada em 26/03/2018.
*/

@Entity
public class CategoriaOcorrencia {

	// Atributos
	@Id
	private Long id;
	
	@Column(length = 30, nullable = false, unique = false)
	private String nome;
	
	// Getters and Setters
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
	
}
